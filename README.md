Use docker-compose command to launch nginx container.

To see the application use http://localhost

Images are located in www/img directory.

X-Cache-Status is added to see cache status.

Results
First two requests will show X-Cache-Status:MISS header as proxy_cache_min_uses 2; direcive is set.

Third request will return X-Cache-Status:HIT header

Run "curl http://localhost/img/img.jpeg -s -I -H "secret-header:true" to drop nginx cache